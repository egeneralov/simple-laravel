# Laravel Quickstart - Basic

## Tutorial quickstart

    composer install
    php artisan migrate
    php artisan serve

See [Complete Tutorial](https://laravel.com/docs/5.2/quickstart) for details.

## Pipeline quickstart


### Set-up VM

0. Fork this project to any gitlab instance (like gitlab.com) with hostname "stage"
1. order VM in cloud for staging.
2. inside VM run commands:

```bash
wget -O - https://raw.githubusercontent.com/egeneralov/docker/master/get.sh | bash -xe
wget -O - https://raw.githubusercontent.com/egeneralov/gitlab-runner/master/get.sh 2>/dev/null | GITLAB_TOKEN=${ENTER_TOKEN_HERE} bash -xe
adduser gitlab-runner docker
```

### Run pipeline

Go to project -> CI/CD -> Pipelines -> Run Pipeline -> button "Run Pipeline"

### Set-up VM for "production"

Warning: suitable only for pet-projects, and require ssl certificate for exclude MitM attack.

Repeat "Set-up" stage, but replace "stage" to "prod".

Modify `.gitlab-ci.yml` - copy job "Staging" with name "Production" and insde replace tag "staging" to "prod".
