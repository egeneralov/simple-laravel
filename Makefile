all: composer down up logs

composer:
	docker run -i --rm -w ${PWD} -v ${PWD}:${PWD} composer:1.8 install --prefer-dist --no-ansi --no-interaction --no-progress --no-scripts

down:
	docker-compose down --remove-orphans
	docker volume rm simple-laravel_dbdata || true

up:
	docker-compose up -d --build

logs:
	docker-compose logs --tail 10 -f
